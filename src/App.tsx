import React from "react";
import "./App.css";
import UnauthorizedLayout from "./containers/UnauthorizedLayout/UnauthorizedLayout";
import { BrowserRouter } from "react-router-dom";
import { Route, Redirect, Switch } from "react-router";
import Login from "./containers/Login/Login";
import { Provider } from "react-redux";
import store from "./utils/store";
import Registration from "./containers/Registration/Registration";
import { ApolloProvider } from "@apollo/react-hoc";
import apolloClient from "./utils/apollo";
import { loginPath, registrationPath, processListPath, userProfilePath } from "./utils/routePaths";
import AuthorizedLayout from "./containers/AuthorizedLayout/AuthorizedLayout";
import UserProfile from "./containers/UserProfile/UserProfile";

const unauthorizedRoutes = (
  <UnauthorizedLayout>
    <Switch>
      <Route path={loginPath} component={Login} />
      <Route path={registrationPath} component={Registration} />
      <Redirect to={loginPath} />
    </Switch>
  </UnauthorizedLayout>
);

const authorizedRoutes = (
  <AuthorizedLayout>
    <Switch>
      <Route path={processListPath}>
        processList
      </Route>
      <Route path={userProfilePath}>
        <UserProfile />
      </Route>
    </Switch>
  </AuthorizedLayout>
);

const isAuthorized = () => !!localStorage.getItem("token"); 

function App() {
  return (
    <ApolloProvider client={apolloClient}>
      <Provider store={store}>
        <BrowserRouter>{isAuthorized() ? authorizedRoutes : unauthorizedRoutes}</BrowserRouter>
      </Provider>
    </ApolloProvider>
  );
}

export default App;
