import { createSelector } from "reselect";

export const notEmptyValidator = (value: string) => {
  if (!value) {
    return "Поле должно быть заполнено";
  }
};

export const repeatPasswordValidatorGetter = createSelector(
  (passwordFieldName: string) => passwordFieldName,
  (passwordFieldName: string) => {
    return (repeatPasswordFieldValue: string, fieldValues: { [key: string]: string }) => {
      if (repeatPasswordFieldValue !== fieldValues[passwordFieldName]) {
        return "Пароль и подтверждение не совпадают";
      }
    };
  }
);

export const emailValidator = (value: string) => {
  if (!/@/.test(value)) {
    return "Некорректный email";
  }
};
