// unauthorizedRoutes
export const loginPath = "/login";
export const registrationPath = "/registration";

// authorizedRoutes
export const processListPath = "/process-list";
export const userProfilePath = "/user-profile";
