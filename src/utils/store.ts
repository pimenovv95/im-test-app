import { createStore, combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

const reducers = combineReducers({
  form: formReducer
});

const store = createStore(
  reducers, 
  undefined,
  // @ts-ignore
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store;