import React from "react";
import { Link, withRouter, RouteComponentProps, matchPath } from "react-router-dom";
import { IAppMenuItemProps } from "./AppMenuItem.types";
import {
  appMenuItemStyleClassName,
  appMenuItemIconStyleClassName,
  activeAppMenuItemStyleClassName
} from "./AppMenuItem.styles";

const AppMenuItem: React.FC<IAppMenuItemProps & RouteComponentProps> = props => {
  const IconComponent = props.iconComponent;

  const classNames = [appMenuItemStyleClassName];

  const isActiveRoute = matchPath(document.location.pathname, { path: props.path, exact: true });
  if (isActiveRoute) {
    classNames.push(activeAppMenuItemStyleClassName);
  }

  return (
    <Link to={props.path} className={classNames.join(" ")}>
      <IconComponent className={appMenuItemIconStyleClassName} />
      {props.caption}
    </Link>
  );
};

export default withRouter(AppMenuItem);
