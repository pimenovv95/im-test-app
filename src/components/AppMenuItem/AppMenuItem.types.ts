export interface IAppMenuItemProps {
  path: string;
  caption: string;
  iconComponent: React.ComponentType<any>;
}

export interface IAppMenuItemState {}
