import { style } from "typestyle";

export const appMenuItemStyleClassName = style({
  padding: "18px 24px",
  display: "block",
  color: "#FFFFFF",
  textDecoration: "none",
  fontWeight: "bold",
  fontSize: "14px",
  lineHeight: "24px"
});

export const activeAppMenuItemStyleClassName = style({
  background: "#333350",
  color: "#FDE078",
  borderLeft: "4px solid #FDE078",
  paddingLeft: "20px"
});

export const appMenuItemIconStyleClassName = style({
  paddingRight: "12px",
  float: "left"
});
