import React from "react";
import { topPanelStyleClassName } from "./TopPanel.styles";
import AppMenuButton from "../AppMenuButton/AppMenuButton";
import { ITopPanelProps } from "./TopPanel.types";

class TopPanel extends React.PureComponent<ITopPanelProps> {
  render() {
    return (
      <div className={topPanelStyleClassName}>
        <AppMenuButton onClick={this.props.onAppMenuButtonClick}>Меню</AppMenuButton>
      </div>
    );
  }
}

export default TopPanel;
