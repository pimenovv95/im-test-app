import { style } from "typestyle";

export const topPanelStyleClassName = style({
  height: "60px",
  boxShadow: "0px 1px 10px rgba(104, 121, 187, 0.1)",
  background: "#FFFFFF",
  position: "fixed",
  left: 0,
  right: 0,
  top: 0
});