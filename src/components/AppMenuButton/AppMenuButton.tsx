import React from "react";
import { getAppMenuButtonStyleClassName, getAppMenuIconStyleClassName } from "./AppMenuButton.styles";
import { IAppMenuButtonProps } from "./AppMenuButton.types";
import { ReactComponent as AppMenuIcon } from "../../resources/appMenu.svg";

const AppMenuButton: React.FC<IAppMenuButtonProps> = props => {
  return (
    <div className={getAppMenuButtonStyleClassName(props.textColor, props.backgroundColor)} onClick={props.onClick}>
      <AppMenuIcon className={getAppMenuIconStyleClassName(props.textColor)} />
      <span>{props.children}</span>
    </div>
  );
};

AppMenuButton.defaultProps = {
  textColor: "#6879BB"
};

export default AppMenuButton;
