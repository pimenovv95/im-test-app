import { style } from "typestyle";

export const getAppMenuButtonStyleClassName = (textColor?: string, backgroundColor?: string) =>
  style({
    width: "224px",
    padding: "18px 24px",
    lineHeight: "24px",
    fontSize: "14px",
    fontWeight: "bold",
    color: textColor,
    cursor: "pointer",
    background: backgroundColor,
    height: "60px",
    boxSizing: "border-box"
  });

export const getAppMenuIconStyleClassName = (iconColor?: string) =>
  style({
    float: "left",
    marginRight: "12px",
    fill: iconColor
  });
