export interface IAppMenuButtonProps {
  onClick: React.MouseEventHandler;
  textColor?: string;
  backgroundColor?: string;
}
