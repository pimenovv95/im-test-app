import React from "react";
import { IFormOptionProps } from "./FormOption.types";
import { getLabelStyleClassName, getWrapperStyleClassName } from "./FormOption.styles";

const FormOption: React.FC<IFormOptionProps> = props => {
  return (
    <div>
      <div className={getLabelStyleClassName(props.labelCol)}>{props.label}</div>
      <div className={getWrapperStyleClassName(props.wrapperCol)}>{props.children}</div>
    </div>
  );
};

export default FormOption;
