import { style } from "typestyle";
import { TCol } from "./FormOption.types";

export const getLabelStyleClassName = (labelCol: TCol) =>
  style({
    display: "inline-block",
    width: `${(100 / 12) * labelCol}%`,
    float: "left",
    height: "36px",
    lineHeight: "36px",
    fontSize: "14px",
    color: "#999999"
  });

export const getWrapperStyleClassName = (labelCol: TCol) =>
  style({
    display: "inline-block",
    width: `${(100 / 12) * labelCol}%`
  });
