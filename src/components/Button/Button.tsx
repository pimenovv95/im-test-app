import React from "react";
import { IButtonProps } from "./Button.types";
import { mainStyleClassName } from "./Button.styles";

const Button: React.FC<IButtonProps> = (props) => {
  const classNames = [mainStyleClassName];

  if (props.className) {
    classNames.push(props.className)
  }

  return <button {...props} type="button" className={classNames.join(" ")} />
}

export default Button;