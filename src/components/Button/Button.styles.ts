import { style } from "typestyle";

export const mainStyleClassName = style({
  background: "#FFCE0C",
  padding: "6px 24px",
  fontSize: "12px",
  lineHeight: "24px",
  fontWeight: "bold",
  border: "none",
  borderRadius: "4px",
  cursor: "pointer",
  $nest: {
    "&:focus": {
      boxShadow: "0px 1px 10px #FFD73B",
      outline: "none"
    },
    "&:hover": {
      background: "#FFD73B"
    },
    "&:active": {
      background: "#FFC40C",
      boxShadow: "inset 0px 0px 10px rgba(0, 0, 0, 0.12)"
    }
  }
});
