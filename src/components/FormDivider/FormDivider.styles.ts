import { style } from "typestyle";

export const formDividerStyleClassName = style({
  borderBottom: "1px solid #D6DCE9",
  margin: "24px -24px"
});
