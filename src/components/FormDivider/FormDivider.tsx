import React from "react";
import { formDividerStyleClassName } from "./FormDivider.styles";

const FormDivider: React.FC = () => (
  <div className={formDividerStyleClassName} />
) 

export default FormDivider;