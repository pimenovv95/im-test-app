import { WrappedFieldProps } from "redux-form";

type TInputType = "password" | "text";

export interface IInputProps extends React.HTMLProps<HTMLInputElement>, Partial<WrappedFieldProps> {
  type?: TInputType;
  invalid?: boolean;
}

export interface IInputState {
  type?: TInputType;
}
