import { style } from "typestyle";

export const mainStyleClassName = style({
  padding: "6px 12px",
  width: "100%",
  boxSizing: "border-box" as const,
  border: "1px solid #D6DCE9",
  borderRadius: "4px",
  color: "#6879BB",
  fontSize: "14px",
  lineHeight: "22px",
  $nest: {
    "&:focus": {
      outline: "none",
      border: "1px solid #415FD5"
    },
    "&::-webkit-input-placeholder": {
      color: "#CCCCCC"
    }
  }
});

export const passwordStyleClassName = style({
  paddingRight: "36px"
});

export const invalidStyleClassName = style({
  border: "1px solid #EE4141"
});

export const passIconClassName = style({
  position: "absolute",
  padding: "12px",
  cursor: "pointer",
  right: 0,
  top: 0,
  fill: "#6979BB",
  $nest: {
    "&:hover": {
      fill: "#415FD5"
    }
  }
});

export const wrapperClassName = style({
  position: "relative",
  paddingBottom: "12px"
});

export const errorStyleClassName = style({
  color: "#EE4141",
  fontSize: "14px"
});
