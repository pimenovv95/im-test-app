import React from "react";
import { IInputProps, IInputState } from "./Input.types";
import {
  mainStyleClassName,
  passwordStyleClassName,
  wrapperClassName,
  passIconClassName,
  invalidStyleClassName,
  errorStyleClassName
} from "./Input.styles";
import { ReactComponent as ClosedEyeIcon } from "../../resources/closedEye.svg";
import { ReactComponent as OpenedEyeIcon } from "../../resources/openedEye.svg";

class Input extends React.PureComponent<IInputProps, IInputState> {
  public static defaultProps = {
    type: "text"
  };

  constructor(props: IInputProps) {
    super(props);

    this.state = {
      type: props.type
    };

    this.switchType = this.switchType.bind(this);
  }

  private switchType() {
    this.setState({ type: this.state.type === "password" ? "text" : "password" });
  }

  private getPassIcon() {
    const IconComponent = this.state.type === "password" ? ClosedEyeIcon : OpenedEyeIcon;

    return <IconComponent className={passIconClassName} onClick={this.switchType} />;
  }

  public render() {
    const { type, meta, input, ...restProps } = this.props;

    let inputClassNames = [mainStyleClassName];

    if (type === "password") {
      inputClassNames.push(passwordStyleClassName);
    }

    let error;
    if (meta && meta.touched && meta.invalid) {
      inputClassNames.push(invalidStyleClassName);
      error = meta.error;
    }

    return (
      <div className={wrapperClassName}>
        <input
          {...restProps}
          {...input}
          className={inputClassNames.join(" ")}
          type={this.state.type}
        />
        {type === "password" ? this.getPassIcon() : null}
        {error ? <div className={errorStyleClassName}>{error}</div> : null}
      </div>
    );
  }
}

export default Input;
