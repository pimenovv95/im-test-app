import { style } from "typestyle";

export const appMenuBackgroundStyleClassName = style({
  background: "#000000",
  opacity: "0.5",
  height: "100%",
  position: "fixed",
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  zIndex: 1000
});

export const appMenuItemsContainerClassName = style({
  position: "fixed",
  top: 0,
  left: 0,
  bottom: 0,
  background: "#404064",
  width: "224px",
  height: "100%",
  boxShadow: "0px 0px 24px rgba(0, 0, 0, 0.3)",
  zIndex: 1000
});
