import React from "react";
import AppMenuButton from "../AppMenuButton/AppMenuButton";
import { IAppMenuProps } from "./AppMenu.types";
import { appMenuItemsContainerClassName, appMenuBackgroundStyleClassName } from "./AppMenu.styles";
import { ReactComponent as ProcesetSVG } from "../../resources/proceset.svg";
import AppMenuItem from "../AppMenuItem/AppMenuItem";
import { IAppMenuItemProps } from "../AppMenuItem/AppMenuItem.types";

const AppMenu: React.FC<IAppMenuProps> & { Item: React.ComponentType<IAppMenuItemProps> } = props => {
  return props.show ? (
    <>
      <div className={appMenuBackgroundStyleClassName} onClick={props.onHide} />
      <div className={appMenuItemsContainerClassName}>
        <AppMenuButton textColor="#FFFFFF" backgroundColor="#535374" onClick={props.onHide}>
          <ProcesetSVG />
        </AppMenuButton>
        {props.children}
      </div>
    </>
  ) : null;
};

AppMenu.Item = AppMenuItem;

export default AppMenu;
