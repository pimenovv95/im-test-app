export interface IAppMenuProps {
  onHide: React.MouseEventHandler;
  show: boolean;
}
