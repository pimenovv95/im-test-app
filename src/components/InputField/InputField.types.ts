import { BaseFieldProps } from "redux-form";
import { IInputProps } from "../Input/Input.types";

export interface IInputFieldProps
  extends Omit<BaseFieldProps<IInputProps>, "component">,
    Omit<IInputProps, "onFocus" | "name" | "onBlur" | "onChange" | "onDrop" | "onDragStart" | "ref"> {}
