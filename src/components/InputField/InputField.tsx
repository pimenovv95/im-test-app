import { IInputFieldProps } from "./InputField.types";
import React from "react";
import { Field } from "redux-form";
import Input from "../Input/Input";

const InputField: React.FC<IInputFieldProps> = ({ ...restProps }) => {
  return <Field component={Input} {...restProps} />;
};

export default InputField;
