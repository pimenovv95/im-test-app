import React from "react";
import { mainStyleClassName, errorIconStyleClassName } from "./ErrorNotification.styles";
import { IErrorNotificationProps } from "./ErrorNotification.types";
import { ReactComponent as ErrorNotificationIcon } from "../../resources/errorNotification.svg"

const ErrorNotification: React.FC<IErrorNotificationProps> = (props) => {
  return (
    <div className={mainStyleClassName}>
      <ErrorNotificationIcon className={errorIconStyleClassName} />
      {props.children}
    </div>
  );
};

export default ErrorNotification;