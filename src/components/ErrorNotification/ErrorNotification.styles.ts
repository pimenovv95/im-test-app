import { style } from "typestyle";

export const mainStyleClassName = style({
  position: "relative",
  background: "#FFE9DB",
  padding: "36px 48px 36px 120px",
  margin: "60px -48px 0",
  fontSize: "14px",
  lineHeight: "19px",
  color: "#333333"
});

export const errorIconStyleClassName = style({
  position: "absolute",
  left: "48px",
  top: "24px"
});
