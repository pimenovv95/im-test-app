import gql from "graphql-tag";

const userQuery = gql`
  query User {
    currentUser {
      id
      firstName
      secondName
      email
    }
  }
`;

export default userQuery;
