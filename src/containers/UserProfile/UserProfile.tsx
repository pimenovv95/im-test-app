import React from "react";
import {
  userProfileTitleStyleClassName,
  userProfileSubmitButtonStyleClassName
} from "./UserProfile.styles";
import Button from "../../components/Button/Button";
import UserProfileForm, {
  UserProfileForm as UserProfileFormComponent
} from "./form/UserProfileForm";
import { withApollo } from "@apollo/react-hoc";
import { IUserProfileProps, IUserProfileState } from "./UserProfile.types";
import userQuery from "./query/userQuery";
import { connect } from "react-redux";
import { submit, SubmissionError } from "redux-form";
import updateUserMutation from "./mutations/updateUserMutation";
import shajs from "sha.js";

class UserProfile extends React.PureComponent<IUserProfileProps, IUserProfileState> {
  constructor(props: IUserProfileProps) {
    super(props);

    this.onSubmitButtonClick = this.onSubmitButtonClick.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onFormSubmitSuccess = this.onFormSubmitSuccess.bind(this);

    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    this.props.client
      .query({
        query: userQuery
      })
      .then(({ data }: any) => {
        this.setState({
          loading: false,
          response: data.currentUser
        });
      });
  }

  getTitle() {
    const { response } = this.state;

    const title = `${response?.firstName} ${response?.secondName}. Редактирование`;

    return <span className={userProfileTitleStyleClassName}>{title}</span>;
  }

  onSubmitButtonClick() {
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch(submit(UserProfileFormComponent.formName));
    }
  }

  getSubmitButton() {
    return (
      <Button className={userProfileSubmitButtonStyleClassName} onClick={this.onSubmitButtonClick}>
        Сохранить
      </Button>
    );
  }

  onFormSubmit(formValues: any) {
    return new Promise((resolve, reject) => {
      const passwordValue = formValues[UserProfileFormComponent.formFieldNames.PASSWORD];
      const passwordHash =
        passwordValue &&
        shajs("sha256")
          .update(passwordValue)
          .digest("hex");

      this.props.client
        .mutate({
          mutation: updateUserMutation,
          variables: {
            id: this.state.response?.id,
            email: formValues[UserProfileFormComponent.formFieldNames.EMAIL],
            firstName: formValues[UserProfileFormComponent.formFieldNames.FIRST_NAME],
            secondName: formValues[UserProfileFormComponent.formFieldNames.SECOND_NAME],
            password: passwordHash
          }
        })
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          reject(new SubmissionError({ _error: error.message }));
        });
    });
  }

  onFormSubmitSuccess({ data }: any) {
    this.setState({ response: data.editUser });
  }

  getHeader() {
    return (
      <div>
        {this.getTitle()}
        {this.getSubmitButton()}
      </div>
    );
  }

  getContent() {}

  render() {
    const { loading, response } = this.state;

    if (loading || !response) {
      return "Loading...";
    }

    return (
      <div>
        {this.getHeader()}
        <UserProfileForm
          onSubmit={this.onFormSubmit}
          onSubmitSuccess={this.onFormSubmitSuccess}
          initialValues={{
            [UserProfileFormComponent.formFieldNames.FIRST_NAME]: response.firstName,
            [UserProfileFormComponent.formFieldNames.SECOND_NAME]: response.secondName,
            [UserProfileFormComponent.formFieldNames.EMAIL]: response.email
          }}
        />
      </div>
    );
  }
}

export default withApollo(connect()(UserProfile));
