import { style } from "typestyle";

export const userProfileTitleStyleClassName = style({
  fontSize: "26px",
  lineHeight: "48px",
  color: "#404064",
  fontWeight: "bold"
});

export const userProfileSubmitButtonStyleClassName = style({
  float: "right",
  margin: "6px 0"
});
