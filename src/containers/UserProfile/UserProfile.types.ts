import { WithApolloClient } from "@apollo/react-hoc";
import { Dispatch } from "redux";

export interface IUserProfileProps extends WithApolloClient<{}> {
  dispatch?: Dispatch
}

export interface IUserProfileState {
  loading: boolean;
  response?: {
    id: number;
    firstName: string;
    secondName: string;
    email: string;
  };
}
