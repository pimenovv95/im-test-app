import { InjectedFormProps } from "redux-form";

export interface IUserProfileFormProps extends InjectedFormProps{}