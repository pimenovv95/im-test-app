import { style } from "typestyle";

export const userProfileFormStyleClassName = style({
  background: "#FFFFFF",
  padding: "24px",
  borderRadius: "2px",
  marginTop: "12px"
});
