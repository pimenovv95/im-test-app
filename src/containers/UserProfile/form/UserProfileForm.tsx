import React from "react";
import InputField from "../../../components/InputField/InputField";
import { reduxForm } from "redux-form";
import { IUserProfileFormProps } from "./UserProfileForm.types";
import { notEmptyValidator, emailValidator } from "../../../utils/validators";
import ErrorNotification from "../../../components/ErrorNotification/ErrorNotification";
import { userProfileFormStyleClassName } from "./UserProfileForm.styles";
import FormOption from "../../../components/FormOption/FormOption";
import FormDivider from "../../../components/FormDivider/FormDivider";

export class UserProfileForm extends React.PureComponent<IUserProfileFormProps> {
  public static formFieldNames = {
    ID: "ID",
    FIRST_NAME: "FIRST_NAME",
    SECOND_NAME: "SECOND_NAME",
    EMAIL: "EMAIL",
    PASSWORD: "PASSWORD"
  };

  public static formName = "userProfile";

  private static emailValidators = [notEmptyValidator, emailValidator];

  render() {
    return (
      <div className={userProfileFormStyleClassName}>
        <FormOption labelCol={3} wrapperCol={3} label="Имя">
          <InputField
            name={UserProfileForm.formFieldNames.FIRST_NAME}
            placeholder="Не задано"
            validate={notEmptyValidator}
          />
        </FormOption>
        <FormOption labelCol={3} wrapperCol={3} label="Фамилия">
          <InputField
            name={UserProfileForm.formFieldNames.SECOND_NAME}
            placeholder="Не задано"
            validate={notEmptyValidator}
          />
        </FormOption>
        <FormDivider />
        <FormOption labelCol={3} wrapperCol={3} label="Электронная почта">
          <InputField
            name={UserProfileForm.formFieldNames.EMAIL}
            placeholder="Не задано"
            validate={UserProfileForm.emailValidators}
          />
        </FormOption>
        <FormOption labelCol={3} wrapperCol={3} label="Введите пароль">
          <InputField
            name={UserProfileForm.formFieldNames.PASSWORD}
            type="password"
            placeholder="Не задано"
          />
        </FormOption>
        {this.props.error ? <ErrorNotification>{this.props.error}</ErrorNotification> : null}
      </div>
    );
  }
}

export default reduxForm({ form: UserProfileForm.formName })(UserProfileForm);
