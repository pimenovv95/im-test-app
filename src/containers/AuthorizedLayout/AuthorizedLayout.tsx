import React from "react";
import { layoutStyleClassName } from "./AuthorizedLayout.styles";
import TopPanel from "../../components/TopPanel/TopPanel";
import { IAuthorizedLayoutProps, IAuthorizedLayoutState } from "./AuthorizedLayout.types";
import AppMenu from "../../components/AppMenu/AppMenu";
import { userProfilePath, processListPath } from "../../utils/routePaths";
import { ReactComponent as UserSVG } from "../../resources/user.svg";
import { ReactComponent as AnalyticSVG } from "../../resources/analytic.svg";

export class AuthorizedLayout extends React.PureComponent<
  IAuthorizedLayoutProps,
  IAuthorizedLayoutState
> {
  constructor(props: IAuthorizedLayoutProps) {
    super(props);

    this.showAppMenu = this.showAppMenu.bind(this);
    this.hideAppMenu = this.hideAppMenu.bind(this);

    this.state = {
      showAppMenu: false
    };
  }

  showAppMenu() {
    this.setState({ showAppMenu: true });
  }

  hideAppMenu() {
    this.setState({ showAppMenu: false });
  }

  render() {
    return (
      <div className={layoutStyleClassName}>
        <TopPanel onAppMenuButtonClick={this.showAppMenu} />
        <AppMenu onHide={this.hideAppMenu} show={this.state.showAppMenu}>
          <AppMenu.Item path={userProfilePath} caption="Username" iconComponent={UserSVG} />
          <AppMenu.Item
            path={processListPath}
            caption="Список процессов"
            iconComponent={AnalyticSVG}
          />
        </AppMenu>
        {this.props.children}
      </div>
    );
  }
}

export default AuthorizedLayout;
