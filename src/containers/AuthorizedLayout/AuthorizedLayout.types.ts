export interface IAuthorizedLayoutProps {}

export interface IAuthorizedLayoutState {
  showAppMenu: boolean;
}
