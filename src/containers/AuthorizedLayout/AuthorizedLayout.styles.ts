import { style } from "typestyle";

export const layoutStyleClassName = style({
  padding: "84px 24px 24px 24px",
  background: "#EBF2FB",
  minHeight: "100%",
  boxSizing: "border-box"
});
