import React from "react";
import { mainStyleClassName } from "./Title.styles";

const Title: React.FC = ({ children }) => {
  return <div className={mainStyleClassName}>{children}</div>;
};

export default Title;
