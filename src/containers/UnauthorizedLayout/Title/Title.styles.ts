import { style } from "typestyle";

export const mainStyleClassName = style({
  fontSize: "18px",
  fontWeight: "bold",
  color: "#404064",
  marginBottom: "22px"
});
