import React from "react";
import { backgroundStyle, logoStyle, formLayoutStyle } from "./UnauthorizedLayout.styles";
import { ReactComponent as SvgLogo } from "../../resources/logo.svg";
import Title from "./Title/Title";

const UnauthorizedLayout: React.FC & { Title: React.ComponentType } = ({ children }) => (
  <>
    <div style={backgroundStyle} />
    <div style={logoStyle}>
      <SvgLogo />
    </div>
    <div style={formLayoutStyle}>{children}</div>
  </>
);

UnauthorizedLayout.Title = Title;

export default UnauthorizedLayout;
