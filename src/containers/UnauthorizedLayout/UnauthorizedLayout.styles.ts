export const backgroundStyle = {
    background: "linear-gradient(213.53deg, #6879BB 7.06%, #9300BB 95.23%)",
    width: "100%",
    height: "100%",
    position: "fixed" as const,
    top: 0,
    zIndex: -1
}

export const logoStyle = {
    textAlign: "center" as const,
    padding: "32px 0",
    width: "100%",
    position: "absolute" as const,
    top: 0
}

export const formLayoutStyle = {
    width: "424px",
    margin: "auto",
    top: "112px",
    minHeight: "calc(100% - 112px)",
    position: "relative" as const,
    background: "#FFFFFF",
    padding: "32px 48px",
    boxSizing: "border-box" as const,
    borderRadius: "2px 2px 0 0"
}