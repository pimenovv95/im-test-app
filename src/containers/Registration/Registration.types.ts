import { WithApolloClient } from "@apollo/react-hoc";
import { RouteComponentProps } from "react-router";

export interface IRegistrationProps extends WithApolloClient<RouteComponentProps<{}>>{}