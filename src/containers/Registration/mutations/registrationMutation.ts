import gql from "graphql-tag";

const registrationMutation = gql`
  mutation Registration(
    $firstName: String!
    $secondName: String!
    $email: String!
    $password: String!
  ) {
    signup(firstName: $firstName, secondName: $secondName, email: $email, password: $password)
  }
`;

export default registrationMutation;
