import { style } from "typestyle";

export const registrationButtonStyleClassName = style({ width: "100%", marginTop: "12px" });
