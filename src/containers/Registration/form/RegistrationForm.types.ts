import { InjectedFormProps } from "redux-form";
import { Dispatch } from "redux";

export interface IRegistrationFormProps extends InjectedFormProps {
  dispatch?: Dispatch;
}