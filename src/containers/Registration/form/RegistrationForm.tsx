import React from "react";
import InputField from "../../../components/InputField/InputField";
import { reduxForm, submit } from "redux-form";
import { IRegistrationFormProps } from "./RegistrationForm.types";
import Button from "../../../components/Button/Button";
import { registrationButtonStyleClassName } from "./RegistrationForm.styles";
import {
  notEmptyValidator,
  repeatPasswordValidatorGetter,
  emailValidator
} from "../../../utils/validators";
import ErrorNotification from "../../../components/ErrorNotification/ErrorNotification";

export class RegistrationForm extends React.PureComponent<IRegistrationFormProps> {
  public static formFieldNames = {
    FIRST_NAME: "FIRST_NAME",
    SECOND_NAME: "SECOND_NAME",
    EMAIL: "EMAIL",
    PASSWORD: "PASSWORD",
    REPEAT_PASSWORD: "REPEAT_PASSWORD"
  };

  public static formName = "registration";

  private static emailValidators = [notEmptyValidator, emailValidator];
  private static repeatPasswordValidators = [
    notEmptyValidator,
    repeatPasswordValidatorGetter(RegistrationForm.formFieldNames.PASSWORD)
  ];

  constructor(props: IRegistrationFormProps) {
    super(props);

    this.onSubmitButtonClick = this.onSubmitButtonClick.bind(this);
  }

  onSubmitButtonClick() {
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch(submit(RegistrationForm.formName));
    }
  }

  render() {
    return (
      <>
        <InputField
          name={RegistrationForm.formFieldNames.FIRST_NAME}
          placeholder="Имя"
          validate={notEmptyValidator}
        />
        <InputField
          name={RegistrationForm.formFieldNames.SECOND_NAME}
          placeholder="Фамилия"
          validate={notEmptyValidator}
        />
        <InputField
          name={RegistrationForm.formFieldNames.EMAIL}
          placeholder="Электронная почта"
          validate={RegistrationForm.emailValidators}
        />
        <InputField
          name={RegistrationForm.formFieldNames.PASSWORD}
          type="password"
          placeholder="Пароль"
          validate={notEmptyValidator}
        />
        <InputField
          name={RegistrationForm.formFieldNames.REPEAT_PASSWORD}
          type="password"
          placeholder="Повторите пароль"
          validate={RegistrationForm.repeatPasswordValidators}
        />
        <Button onClick={this.onSubmitButtonClick} className={registrationButtonStyleClassName}>
          Применить и войти
        </Button>
        {this.props.error ? <ErrorNotification>{this.props.error}</ErrorNotification> : null}
      </>
    );
  }
}

export default reduxForm({ form: RegistrationForm.formName })(RegistrationForm);
