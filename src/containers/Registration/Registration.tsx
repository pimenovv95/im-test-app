import React from "react";
import RegistrationForm, {
  RegistrationForm as RegistrationFormComponent
} from "./form/RegistrationForm";
import registrationMutation from "./mutations/registrationMutation";
import { IRegistrationProps } from "./Registration.types";
import { SubmissionError } from "redux-form";
import UnauthorizedLayout from "../UnauthorizedLayout/UnauthorizedLayout";
import shajs from "sha.js";
import { withApollo } from "@apollo/react-hoc";
import { withRouter } from "react-router";
import { processListPath } from "../../utils/routePaths";

class Registration extends React.PureComponent<IRegistrationProps> {
  constructor(props: IRegistrationProps) {
    super(props);

    this.onSubmitSuccess = this.onSubmitSuccess.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(fields: any) {
    return new Promise((resolve, reject) => {
      const passwordHash = shajs('sha256').update(fields[RegistrationFormComponent.formFieldNames.PASSWORD]).digest('hex');

      this.props.client
        .mutate({
          mutation: registrationMutation,
          variables: {
            firstName: fields[RegistrationFormComponent.formFieldNames.FIRST_NAME],
            secondName: fields[RegistrationFormComponent.formFieldNames.SECOND_NAME],
            email: fields[RegistrationFormComponent.formFieldNames.EMAIL],
            password: passwordHash
          }
        })
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          reject(new SubmissionError({ _error: error.message }));
        });
    });
  }

  onSubmitSuccess({ data }: any) {
    localStorage.setItem("token", data.signup);
    this.props.history.push(processListPath);
  }

  render() {
    return (
      <>
        <UnauthorizedLayout.Title>Задайте электронную почту и пароль для администратора системы</UnauthorizedLayout.Title>
        <RegistrationForm onSubmit={this.onSubmit} onSubmitSuccess={this.onSubmitSuccess} />
      </>
    );
  }
}

export default withApollo(withRouter(Registration) as any);
