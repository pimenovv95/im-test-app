import { RouteComponentProps } from "react-router";
import { WithApolloClient } from "@apollo/react-hoc";

export interface ILoginProps extends WithApolloClient<RouteComponentProps<{}>> {}
