import { InjectedFormProps } from "redux-form";
import { Dispatch } from "redux";

export interface ILoginFormProps extends InjectedFormProps {
  dispatch?: Dispatch;
}