import React from "react";
import InputField from "../../../components/InputField/InputField";
import { reduxForm, submit } from "redux-form";
import { ILoginFormProps } from "./LoginForm.types";
import Button from "../../../components/Button/Button";
import { loginButtonStyleClassName } from "./LoginForm.styles";
import { notEmptyValidator } from "../../../utils/validators";
import ErrorNotification from "../../../components/ErrorNotification/ErrorNotification";

export class LoginForm extends React.PureComponent<ILoginFormProps> {
  public static formFieldNames = {
    EMAIL: "EMAIL",
    PASSWORD: "PASSWORD"
  };

  public static formName = "login";

  constructor(props: ILoginFormProps) {
    super(props);

    this.onSubmitButtonClick = this.onSubmitButtonClick.bind(this);
  }

  onSubmitButtonClick() {
    const { dispatch } = this.props;
    if (dispatch) {
      dispatch(submit(LoginForm.formName));
    }
  }

  render() {
    return (
      <>
        <InputField
          name={LoginForm.formFieldNames.EMAIL}
          placeholder="Электронная почта"
          validate={notEmptyValidator}
        />
        <InputField
          name={LoginForm.formFieldNames.PASSWORD}
          type="password"
          placeholder="Пароль"
          validate={notEmptyValidator}
        />
        <Button onClick={this.onSubmitButtonClick} className={loginButtonStyleClassName}>
          Войти в систему
        </Button>
        {this.props.error ? <ErrorNotification>{this.props.error}</ErrorNotification> : null}
      </>
    );
  }
}

export default reduxForm({ form: LoginForm.formName })(LoginForm);
