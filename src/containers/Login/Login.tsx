import React from "react";
import LoginForm, { LoginForm as LoginFormComponent } from "./form/LoginForm";
import loginMutation from "./mutations/loginMutation";
import { ILoginProps } from "./Login.types";
import { SubmissionError } from "redux-form";
import shajs from "sha.js";
import { withApollo } from "@apollo/react-hoc";
import { withRouter } from "react-router";
import { processListPath } from "../../utils/routePaths";

class Login extends React.PureComponent<ILoginProps> {
  constructor(props: ILoginProps) {
    super(props);

    this.onSubmitSuccess = this.onSubmitSuccess.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(fields: any) {
    return new Promise((resolve, reject) => {
      const passwordHash = shajs("sha256")
        .update(fields[LoginFormComponent.formFieldNames.PASSWORD])
        .digest("hex");

      this.props.client
        .mutate({
          mutation: loginMutation,
          variables: {
            email: fields[LoginFormComponent.formFieldNames.EMAIL],
            password: passwordHash
          }
        })
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          reject(new SubmissionError({ _error: error.message }));
        });
    });
  }

  onSubmitSuccess({ data }: any) {
    localStorage.setItem("token", data.login.token);
    this.props.history.push(processListPath);
  }

  render() {
    return <LoginForm onSubmit={this.onSubmit} onSubmitSuccess={this.onSubmitSuccess} />;
  }
}

export default withApollo(withRouter(Login) as any);
